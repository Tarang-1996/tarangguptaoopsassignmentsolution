package com.department.parent;

public class SuperDepartment {

	/**
	 * to return the departmentName
	 * 
	 * @return String
	 */
	public String departmentName() {
		return "Super Department";
	}

	/**
	 * to return the Today's work
	 * 
	 * @return String
	 */
	public String getTodaysWork() {
		return "No Work as of now";
	}

	/**
	 * to return the dead line of work
	 * 
	 * @return String
	 */
	public String getWorkDeadline() {
		return "Nil";
	}

	/**
	 * to know the holiday
	 * 
	 * @return String
	 */
	public String isTodayAHoliday() {
		return "Today is not a holiday";
	}

}
