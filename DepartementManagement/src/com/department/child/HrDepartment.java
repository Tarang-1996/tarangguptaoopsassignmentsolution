package com.department.child;

import com.department.parent.SuperDepartment;

public class HrDepartment extends SuperDepartment {
	
	/**
	 * to return the department name
	 * 
	 * @return String
	 */
	public String departmentName() {
		return "HR Department";
	}

	/**
	 * to return the today's work
	 * 
	 * @return String
	 */
	public String getTodaysWork() {
		return "Fill today�s worksheet and mark your attendance";
	}

	/**
	 * to return the work deadline
	 * 
	 * @return String
	 */
	public String getWorkDeadline() {
		return "Complete by EOD";
	}

	/**
	 * to return the activities planned
	 * 
	 * @return String
	 */
	public String doActivity() {
		return "team Lunch";
	}

}
