package com.department.child;

import com.department.parent.SuperDepartment;

public class AdminDepartment extends SuperDepartment {

	/**
	 * to return the department name
	 * 
	 * @return String
	 */
	public String departmentName() {
		return "Admin Department";
	}

	/**
	 * to return the today's work
	 * 
	 * @return String
	 */
	public String getTodaysWork() {
		return "Complete your documents Submission";
	}

	/**
	 * to return the work deadline
	 * 
	 * @return String
	 */
	public String getWorkDeadline() {
		return "Complete by EOD";
	}

}
