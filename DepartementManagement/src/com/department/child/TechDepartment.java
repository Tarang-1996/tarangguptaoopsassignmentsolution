package com.department.child;

import com.department.parent.SuperDepartment;

public class TechDepartment extends SuperDepartment {

	/**
	 * to return the department name
	 * 
	 * @return String
	 */
	public String departmentName() {
		return "Tech Department";
	}

	/**
	 * to return the today's work
	 * 
	 * @return String
	 */
	public String getTodaysWork() {
		return "Complete coding of module 1";
	}

	/**
	 * to return the work deadline
	 * 
	 * @return String
	 */
	public String getWorkDeadline() {
		return "Complete by EOD";
	}

	/**
	 * to return the technology stack information
	 * 
	 * @return String
	 */
	public String getTechStackInformation() {
		return "core Java";
	}

}
