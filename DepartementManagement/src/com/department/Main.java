/**
 * 
 */
package com.department;

import com.department.child.AdminDepartment;
import com.department.child.HrDepartment;
import com.department.child.TechDepartment;

/**
 * @author Tarang Gupta
 *
 */
public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		AdminDepartment adminDepartmentObj = new AdminDepartment();
		System.out.println("Welcome to "+adminDepartmentObj.departmentName());
		System.out.println(adminDepartmentObj.getTodaysWork());
		System.out.println(adminDepartmentObj.getWorkDeadline());
		System.out.println(adminDepartmentObj.isTodayAHoliday());
			
		
		HrDepartment hrDepartmentObj = new HrDepartment();
		System.out.println("\nWelcome to "+hrDepartmentObj.departmentName());
		System.out.println(hrDepartmentObj.doActivity());
		System.out.println(hrDepartmentObj.getTodaysWork());
		System.out.println(hrDepartmentObj.getWorkDeadline());
		System.out.println(hrDepartmentObj.isTodayAHoliday());

		
		TechDepartment techDepartmentObj = new TechDepartment();
		System.out.println("\nWelcome to "+techDepartmentObj.departmentName());
		System.out.println(techDepartmentObj.getTodaysWork());
		System.out.println(techDepartmentObj.getWorkDeadline());
		System.out.println(techDepartmentObj.getTechStackInformation());
		System.out.println(techDepartmentObj.isTodayAHoliday());


	}

}
